import json
import urllib2
import random
import os
from itertools import combinations
from scipy import stats

url_h = urllib2.urlopen('http://csbl.bmb.uga.edu/DOOR/search_ajax.php?keyword=pao1&mode=DataTable&sEcho=4&iColumns=6&sColumns=&iDisplayStart=0&iDisplayLength=6000&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4&mDataProp_5=5&_=1384798560566', timeout=5)
obj = json.loads(url_h.read())

operons = []
for item in obj['aaData']:
    operons.append(item[3].split('; '))

def pcl_to_dict(fname):
    expr_data = {}
    pcl_file = open(fname)
    pcl_file.next()#skip header
    for line in pcl_file:
        toks = line.strip().split('\t')
        gid = toks.pop(0).split('_')[0]
        if gid == 'Pae' or gid == 'ig' or gid.startswith('AFFX'):
            continue #skip controls/etc
        exprs = [float(x) for x in toks]
        expr_data[gid] = exprs
    pcl_file.close()
    return expr_data

expr_data = pcl_to_dict('data/pcls/all-pseudomonas.pcl')

gids = expr_data.keys()
out_fh = open('op_combos.tdt', 'w')
out_fh.write('\t'.join(('G1', 'G2', 'OP', 'R')) + '\n')
for operon in operons:
    if len(operon) < 3:
        continue
    for combo in combinations(operon, 2):
        try:
            d1 = expr_data[combo[0]]
            d2 = expr_data[combo[1]]
        except KeyError:
            continue
        combo_r = stats.pearsonr(d1, d2)[0]
        out_fh.write('\t'.join((combo[0], combo[1], 'True', str(combo_r))) + '\n')
        for gene in combo:
            rand_gene = random.choice(gids)
            rand_r = stats.pearsonr(expr_data[gene], expr_data[rand_gene])[0]
            out_fh.write('\t'.join((gene, rand_gene, 'False', str(rand_r))) + '\n')

out_fh.close()

datadir_fnames = os.listdir('data/pcls')

expt_pcl_files = []
for item in datadir_fnames:
    if item.startswith('E-'):
        if item.endswith('.pcl'):
            expt_pcl_files.append(item)

out_fh = open('dset_op_combo.tdt', 'w')
out_fh.write('\t'.join(('Dataset', 'Operon', 'G1', 'G2', 'R')) + '\n')
for fname in expt_pcl_files:
    expt_data = pcl_to_dict(os.path.join('data', fname))
    dname = fname.split('.')[0]
    for operon in operons:
        if len(operon) < 3:
            continue
        operon_name = ';'.join(operon)
        for combo in combinations(operon, 2):
            try:
                d1 = expt_data[combo[0]]
                d2 = expt_data[combo[1]]
            except KeyError:
                continue
            if len(d1) < 5:
                continue
            combo_r = stats.pearsonr(d1, d2)[0]
            out_fh.write('\t'.join((dname, operon_name, combo[0], combo[1], str(combo_r))) + '\n')

out_fh.close()


mkdir -p data
mkdir -p data/zips

for x in `python get_pseudo.py`; do wget -N -P data/zips/ $x ; done

mkdir -p data/cels
mkdir -p data/cels/all-pseudomonas
for x in data/zips/*; do unzip -n $x -d data/cels/all-pseudomonas; done
for x in data/zips/*; do mkdir -p data/cels/`basename -s .raw.1.zip $x`; unzip -n $x -d data/cels/`basename -s .raw.1.zip $x`; done

mkdir -p data/pcls/
for x in data/cels/*; do R --no-save --args $x data/pcls/`basename $x`.pcl < ProcessToPCL.R; done

python get_pseudo_ops.py

mkdir -p figs/
R --no-save < plot_dset_op.R

#These bits are just to make an entrez version, required for PILGRM but not for the rest of the analyses.
wget -N ftp://ftp.ncbi.nih.gov/gene/DATA/GENE_INFO/Archaea_Bacteria/Pseudomonas_aeruginosa_PAO1.gene_info.gz
gunzip -f Pseudomonas_aeruginosa_PAO1.gene_info.gz
python rename_pseudo.py



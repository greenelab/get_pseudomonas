# get_pseudomonas repository

This is some code that we (Casey Greene, Matt Huyck) wrote for some tasks that
we were performing in the lab. This code is not production ready and we are
beginning a project to construct a generalized replacement. We do not recommend
that you use this code. We don't support this code. But we are providing it just
in case it is helpful to someone. The license is BSD 3-clause.

Thanks!
Casey and Matt

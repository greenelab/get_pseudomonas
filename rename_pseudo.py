
symbol_entrez = {}
gi_fh = open('Pseudomonas_aeruginosa_PAO1.gene_info')
gi_fh.next()#skip header
for line in gi_fh:
    toks = line.strip().split('\t')
    entrez = toks[1]
    symbol = toks[3]
    symbol_entrez[symbol] = entrez
gi_fh.close()

in_fh = open('data/pcls/all-pseudomonas.pcl')
out_fh = open('data/pcls/all-pseudomonas.entrez.pcl', 'w')

out_fh.write(in_fh.readline())#write header
for line in in_fh:
    toks = line.strip().split('\t')
    symbol = toks[0].split('_')[0]
    try:
        entrez = symbol_entrez[symbol]
        out_fh.write(entrez + '\t' + '\t'.join(toks[1:]) + '\n')
    except KeyError:
        print("No entrez for " + symbol)

out_fh.close()
in_fh.close()
